﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleCalculator;
using SampleCalculator.Controllers;
using System.Web;
using System.Net.Http;
using Moq;
using System.Net;
using System.Collections.Specialized;

namespace SampleCalculator.Tests.Controllers
{
    [TestClass]
    public class CalculatorControllerTest
    {

        private CalculatorController _calcController;
        private Mock<HttpSessionStateBase> _mockSession;
        private Mock<HttpRequestBase> _mockRequest;

        [TestInitialize]
        public void Setup()
        {
            _mockRequest = new Mock<HttpRequestBase>();
            _mockSession = new Mock<HttpSessionStateBase>();
            var mockHttpContext = new Mock<HttpContextBase>();
            var mockControllerContext = new Mock<ControllerContext>();

            mockHttpContext.Setup(c => c.Request).Returns(_mockRequest.Object);
            mockHttpContext.Setup(c => c.Session).Returns(_mockSession.Object);
            mockControllerContext.Setup(c => c.HttpContext).Returns(mockHttpContext.Object);

            _calcController = new CalculatorController();
            _calcController.ControllerContext = mockControllerContext.Object;
        }

        [TestMethod]
        public void Index()
        {
            // Arrange
            var queryString = new NameValueCollection();            
            _mockRequest.Setup(r => r.Params).Returns(queryString);
            _mockRequest.Setup(r => r.Form).Returns(queryString);

            // Act
            ViewResult result = _calcController.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void Addition()
        {
            // Arrange
            var queryString = new NameValueCollection();
            queryString.Add("txt", "6+6");           
            _mockRequest.Setup(r => r.Params).Returns(queryString);
            _mockRequest.Setup(r => r.Form).Returns(queryString);
            
            // Act
            ViewResult result = _calcController.Index() as ViewResult;
           
            // Assert
            Assert.AreEqual("12", result.ViewBag.result);
            Assert.AreEqual(1, result.ViewBag.flag);
        }

        [TestMethod]
        public void Subtract()
        {
            // Arrange
            var queryString = new NameValueCollection();
            queryString.Add("txt", "18-6");           
            _mockRequest.Setup(r => r.Params).Returns(queryString);
            _mockRequest.Setup(r => r.Form).Returns(queryString);
            

            // Act
            ViewResult result = _calcController.Index() as ViewResult;

            // Assert
            Assert.AreEqual("12", result.ViewBag.result);
            Assert.AreEqual(1, result.ViewBag.flag);
        }

        [TestMethod]
        public void Multiplication()
        {
            // Arrange
            var queryString = new NameValueCollection();
            queryString.Add("txt", "6*6");
            _mockRequest.Setup(r => r.Params).Returns(queryString);
            _mockRequest.Setup(r => r.Form).Returns(queryString);


            // Act
            ViewResult result = _calcController.Index() as ViewResult;

            // Assert
            Assert.AreEqual("36", result.ViewBag.result);
            Assert.AreEqual(1, result.ViewBag.flag);
        }

        [TestMethod]
        public void Division()
        {
            // Arrange
            var queryString = new NameValueCollection();
            queryString.Add("txt", "18/6");
            _mockRequest.Setup(r => r.Params).Returns(queryString);
            _mockRequest.Setup(r => r.Form).Returns(queryString);


            // Act
            ViewResult result = _calcController.Index() as ViewResult;

            // Assert
            Assert.AreEqual("3", result.ViewBag.result);
            Assert.AreEqual(1, result.ViewBag.flag);
        }
    }
}
